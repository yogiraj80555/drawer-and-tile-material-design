package in.codecore.yogiraj.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


public class CardContentFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        RecyclerView recyclerView = (RecyclerView)inflater.inflate(R.layout.recycler_view,container,false);
        recyclerView.setPadding(10,10,10,10);
        recyclerView.setHasFixedSize(true);
        MyContentAdapter contentAdapter = new MyContentAdapter(recyclerView.getContext());
        recyclerView.setAdapter(contentAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return recyclerView;
    }

    /////////////////////////////////////////////////////////////

    public static class ViewHolderClass extends RecyclerView.ViewHolder{
        public ImageView picture;
        public TextView name,description;
        public ViewHolderClass(LayoutInflater inflater, ViewGroup parent){
            super(inflater.inflate(R.layout.fragment_card_content,parent,false));
            picture = (ImageView)itemView.findViewById(R.id.card_image);
            name = (TextView)itemView.findViewById(R.id.card_title);
            description = (TextView)itemView.findViewById(R.id.card_text);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, DetailActivity.class);
                    intent.putExtra(DetailActivity.EXTRA_POSITION, getAdapterPosition());
                    context.startActivity(intent);
                }
            });

        }
    }

    public static class MyContentAdapter extends RecyclerView.Adapter<ViewHolderClass>{
        private static final int LENGTH= 18;
        private final String[] mPlace;
        private final String[] mPlaceDesc;
        private final Drawable[] mPlacePicture;
         public MyContentAdapter(Context ctx){
             Resources resources = ctx.getResources();
             mPlace = resources.getStringArray(R.array.places);
             mPlaceDesc = resources.getStringArray(R.array.place_desc);
             TypedArray array = resources.obtainTypedArray(R.array.places_picture);
             mPlacePicture = new Drawable[array.length()];
             for(int i=0;i<array.length();i++){
                 mPlacePicture[i] = array.getDrawable(i);
             }
array.recycle();
        }

        @NonNull
        @Override
        public ViewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new ViewHolderClass(LayoutInflater.from(viewGroup.getContext()),viewGroup);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolderClass viewHolderClass, int i) {
            viewHolderClass.name.setText(mPlace[i%mPlace.length]);
            viewHolderClass.description.setText(mPlaceDesc[i%mPlaceDesc.length]);
            viewHolderClass.picture.setImageDrawable(mPlacePicture[i%mPlacePicture.length]);
        }

        @Override
        public int getItemCount() {
            return LENGTH;
        }

    }


}

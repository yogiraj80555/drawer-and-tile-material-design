package in.codecore.yogiraj.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;



public class ListContentFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       RecyclerView recyclerView = (RecyclerView)inflater.inflate(R.layout.recycler_view, container,false);
       MyContentAdapter contentAdapter = new MyContentAdapter(recyclerView.getContext());
       recyclerView.setAdapter(contentAdapter);
       recyclerView.setHasFixedSize(true);
       recyclerView.setPadding(10,10,10,10);
       recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
       return recyclerView;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    public static class ViewHolderClass extends RecyclerView.ViewHolder{
        public ImageView avatar;
        public TextView name, description;
        public ViewHolderClass(LayoutInflater inflater, ViewGroup group) {
            super(inflater.inflate(R.layout.fragment_list_content,group,false));
            avatar = (ImageView)itemView.findViewById(R.id.img_list);
            name = (TextView) itemView.findViewById(R.id.listTitle);
            description = (TextView) itemView.findViewById(R.id.list_desc);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, DetailActivity.class);
                    intent.putExtra(DetailActivity.EXTRA_POSITION, getAdapterPosition());
                    context.startActivity(intent);
                }
            });
        }
    }


    public static class MyContentAdapter extends RecyclerView.Adapter<ViewHolderClass>{

        private static final int LENGTH = 18;
        private final String[] mPlaces;
        private final String[] mDescriptions;
        private final Drawable[] mPlacesAvtars;


        public MyContentAdapter(Context context){

            Resources resources = context.getResources();
            mPlaces = resources.getStringArray(R.array.places);
            mDescriptions = resources.getStringArray(R.array.place_desc);
            TypedArray array = resources.obtainTypedArray(R.array.place_avator);
            mPlacesAvtars = new Drawable[array.length()];
            for(int i =0;i<array.length();i++){
                mPlacesAvtars[i] = array.getDrawable(i);
            }
            array.recycle();
        }

        @NonNull
        @Override
        public ViewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new ViewHolderClass(LayoutInflater.from(viewGroup.getContext()),viewGroup);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolderClass viewHolderClass, int i) {

            viewHolderClass.avatar.setImageDrawable(mPlacesAvtars[i%mPlacesAvtars.length]);
            viewHolderClass.description.setText(mDescriptions[i%mDescriptions.length]);
            viewHolderClass.name.setText(mPlaces[i%mPlaces.length]);
        }

        @Override
        public int getItemCount() {
            return LENGTH;
        }
    }


}
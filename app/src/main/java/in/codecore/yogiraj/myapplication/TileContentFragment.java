package in.codecore.yogiraj.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TileContentFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TileContentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TileContentFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        RecyclerView recyclerView = (RecyclerView)inflater.inflate(R.layout.recycler_view, container, false);
        MyContentAdapter contentAdapter = new MyContentAdapter(recyclerView.getContext());
        recyclerView.setAdapter(contentAdapter);
        recyclerView.setHasFixedSize(true);
         int tilepadding = 10;
         recyclerView.setPadding(tilepadding,tilepadding,tilepadding,tilepadding);
         recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
         return recyclerView;
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static class ViewHolderClass extends RecyclerView.ViewHolder{

        public ImageView picture;
        public TextView name;
        public ViewHolderClass(LayoutInflater inflater, ViewGroup group) {
            super(inflater.inflate(R.layout.fragment_tile_content,group,false));
            picture = (ImageView)itemView.findViewById(R.id.tile_picture);
            name = (TextView)itemView.findViewById(R.id.tile_title);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, DetailActivity.class);
                    intent.putExtra(DetailActivity.EXTRA_POSITION, getAdapterPosition());
                    context.startActivity(intent);
                }
            });
        }
    }


    public static class MyContentAdapter extends RecyclerView.Adapter<ViewHolderClass>{

        private static final int LENGTH = 18;
        private final String[] mPlace;
        private final Drawable[] mPicturePlace;

        public MyContentAdapter(Context context){
            Resources resources = context.getResources();
            mPlace = resources.getStringArray(R.array.places);
            TypedArray array = resources.obtainTypedArray(R.array.places_picture);
            mPicturePlace = new Drawable[array.length()];
            for(int i =0; i<array.length(); i++){
                mPicturePlace[i] = array.getDrawable(i);
            }
            array.recycle();
        }

        @NonNull
        @Override
        public ViewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new ViewHolderClass(LayoutInflater.from(viewGroup.getContext()),viewGroup);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolderClass viewHolderClass, int i) {
            viewHolderClass.picture.setImageDrawable(mPicturePlace[i%mPicturePlace.length]);
            viewHolderClass.name.setText(mPlace[i%mPlace.length]);
        }

        @Override
        public int getItemCount() {
            return LENGTH;
        }

    }
}
